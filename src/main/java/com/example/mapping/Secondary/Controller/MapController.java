package com.example.mapping.Secondary.Controller;

import com.example.mapping.Primary.Model.Main;
import com.example.mapping.Primary.Repository.MainRepository;
import com.example.mapping.Secondary.Model.MapClass;
import com.example.mapping.Secondary.Model.MapFunction;
import com.example.mapping.Secondary.Repository.MapRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;


@RestController
@RequestMapping("/map")
public class MapController {

    private MapRepository repo;
    private MainRepository repo1;

    public MapController(MapRepository repo, MainRepository repo1) {
        this.repo = repo;
        this.repo1 = repo1;
    }

    @GetMapping(value = "/all")
    public List<Main> getAllMap()
    {
        return repo1.findAll();
    }



    @GetMapping(value = "/api/{api_id}")
    public Object getAllMap3(@PathVariable int api_id)
    {
        List<MapClass> a = repo.findAll();
        List<Main> b = repo1.findAll();
        MapFunction d = new MapFunction();
        return d.update(a,b,a.get(api_id).getApi_id());
    }

}
